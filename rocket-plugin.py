import weechat
import asyncio
import websockets
import json
import threading
import queue
import time

username = 'kempe'
sha256_password = ''

ws = None
buffer = None
messages = queue.Queue()
halt = False

@asyncio.coroutine
def login():
    global ws
    ws = yield from websockets.connect('ws://test-hx.lysator.liu.se:3000/websocket')
    result = yield from ws.recv()
    weechat.prnt("", result)
    connect_msg = {
            'msg':'connect',
            'version':'1',
            'support': ['1', 'pre2', 'pre1' ]
            }
    yield from ws.send(json.dumps(connect_msg))
    con_res = yield from ws.recv()
    weechat.prnt("", con_res)

    login_msg = {
            'msg': 'method',
            'method':'login',
            'id':'42',
            'params': [
                {
                    'user': { 'username': username },
                    'password': {
                        'digest': sha256_password,
                        'algorithm':'sha-256'
                        }
                }
                ]
                }
    yield from ws.send(json.dumps(login_msg))
    result = yield from ws.recv()
    weechat.prnt("", result)

    room_msg = {
            'msg':'method',
            'method':'rooms/get',
            'id':'42',
            'params':[{'$date': 0}]
            }

    yield from ws.send(json.dumps(room_msg))
    result = yield from ws.recv()
    weechat.prnt("", result)

    sub_msg = {
            "msg": "sub",
            "id": "32",
            "name": "stream-room-messages",
            "params":[
                "GENERAL",
                False
                ]
            }

    yield from ws.send(json.dumps(sub_msg))

@asyncio.coroutine
def receive():
    global halt
    while not halt:
        result = yield from ws.recv()
        msg_dict = json.loads(result)
        if msg_dict.get('msg') == 'ping':
            ping_msg = { 'msg': 'pong' }
            yield from ws.send(json.dumps(ping_msg))

        try:
            dict_res = json.loads(result)
            for item in dict_res['fields']['args']:
                messages.put('{}: {}'.format(item['u']['username'], item['msg']))
        except KeyError:
            pass

def runner():
    loop = asyncio.SelectorEventLoop()
    asyncio.set_event_loop(loop)
    asyncio.get_event_loop().run_until_complete(login())
    asyncio.get_event_loop().run_until_complete(receive())

def timer_cb(data, remaining_calls):
    try:
        result = messages.get(block=False)
        weechat.prnt(buffer, result)
    except queue.Empty:
        pass
    return weechat.WEECHAT_RC_OK

@asyncio.coroutine
def send_msg(msg):
    msg = {
            'msg':'method',
            'method':'sendMessage',
            'id':'42',
            'params': [
                {
                    '_id':'{}'.format(time.time()),
                    'rid':'GENERAL',
                    'msg': '{}'.format(msg)
                }
            ]
           }
    yield from ws.send(json.dumps(msg))

def buffer_input_cb(data, buffer, input_data):
    asyncio.get_event_loop().run_until_complete(send_msg(input_data))
    return weechat.WEECHAT_RC_OK

def buffer_close_cb(data, buffer):
    global halt
    halt = True
    return weechat.WEECHAT_RC_OK

weechat.register("RocketChat", "kempe", "0", "WTFPL", "", "", "")

buffer = weechat.buffer_new("rocketbuffer", "buffer_input_cb", "", "buffer_close_cb", "")
weechat.buffer_set(buffer, "General", "This is the RocketChat General channel")

threading.Thread(target=runner).start()
weechat.hook_timer(1000, 1000, 0, "timer_cb", "")
weechat.prnt("", "Hej du glade!")
